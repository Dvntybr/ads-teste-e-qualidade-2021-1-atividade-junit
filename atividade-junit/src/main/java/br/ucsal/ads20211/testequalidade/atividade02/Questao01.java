package br.ucsal.ads20211.testequalidade.atividade02;

import java.util.Scanner;

public class Questao01 {

	public static void main(String[] args) {
		obterVerificarExibirPrimo();
	}

	private static void obterVerificarExibirPrimo() {
		Integer n;
		Boolean isPrimo;
		n = obterNumeroFaixa();
		isPrimo = verificarPrimo(n);
		exibirPrimoNaoPrimo(n, isPrimo);
	}

	@SuppressWarnings("resource")
	public static Integer obterNumeroFaixa() {
		Scanner scanner = new Scanner(System.in);
		Integer n;
		while (true) {
			System.out.println("Informe um número inteiro entre 1 e 1000 (intervalo fechado):");
			try {
				n = scanner.nextInt();
				if (n < 1 || n > 1000) {
					System.out.println("Número fora da faixa.");
				} else {
					return n;
				}
			} catch (Exception e) {
				System.out.println("Valor inválido! Informe um número inteiro.");
				if (scanner.hasNextLine()) {
					scanner.nextLine();
				}
			}
		}
	}

	public static Boolean verificarPrimo(Integer n) {
		Integer qtdDivisores = 0;
		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				qtdDivisores++;
			}
		}
		if (qtdDivisores == 2) {
			return true;
		}
		return false;
	}

	public static void exibirPrimoNaoPrimo(Integer n, Boolean isPrimo) {
		if (isPrimo) {
			System.out.println("O número " + n + " é primo");
		} else {
			System.out.println("O número " + n + " NÃO é primo");
		}
	}

}
